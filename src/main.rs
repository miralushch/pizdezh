use anyhow::Result;
use structopt::StructOpt;

#[derive(serde::Serialize, serde::Deserialize)]
struct Message {
	text: String,
}

#[derive(StructOpt)]
#[structopt(about = "listen or send messages")]
enum Options {
	Listen,
	SendMessage { device: String, message: String },
}

fn message_received(_: &mut (), message: &[u8]) -> Result<cborpc::CallResponse> {
	let message: Message = rmp_serde::decode::from_read(message)?;
	eprintln!("Received message: {}", message.text);
	Ok(cborpc::CallResponse { success: true, message: vec![] })
}

async fn server() -> Result<()> {
	env_logger::builder().filter_level(log::LevelFilter::Info).parse_env("RUST_LOG").init();

	log::info!("Creating tcp server");
	let tcp_listener = tokio::net::TcpListener::bind("127.0.0.1:0").await?;
	let port = tcp_listener.local_addr()?.port();
	log::info!("port: {}", port);
	log::info!("Creating SAMv3 session");
	let mut session = solitude::Session::new("stream_server_example", solitude::SessionStyle::Stream).await?;

	log::info!("Forwarding tcp server to i2p");
	session.forward("127.0.0.1", port).await?;

	log::info!("Listening on {}", session.address()?);

	tokio::task::spawn(async move {
		loop {
			let (mut stream, _address) = tcp_listener.accept().await.unwrap();
			tokio::task::spawn(async move {
				let mut stream = tokio::io::BufStream::new(&mut stream);
				let stream_info = solitude::StreamInfo::from_bufread(&mut stream).await.unwrap();
				let (mut reader, mut writer) = tokio::io::split(stream);
				log::info!("connection from {}", stream_info.destination);
				loop {
					let mut responder = cborpc::Responder::new(());
					let protocols = responder.protocols();
					let mut server_protocol = std::collections::HashMap::new();
					server_protocol.insert("send-message".to_string(), message_received as cborpc::CallHandler<()>);
					protocols.insert("pizdezh-0".to_string(), server_protocol);
					let responder = responder.wrap_up();
					while let Some(_response) = cborpc::Responder::answer_call(&responder, &mut reader, &mut writer).await.unwrap() {}
				}
			});
		}
	});

	let _: () = std::future::pending().await;
	Ok(())
}

async fn client(device_address: &str, message: &str) -> Result<()> {
	env_logger::builder().filter_level(log::LevelFilter::Info).parse_env("RUST_LOG").init();
	let mut session = solitude::Session::new("stream_client_example", solitude::SessionStyle::Stream).await?;
	let destination = session.look_up(device_address).await?;

	log::info!("Connecting to server");
	let stream = session.connect_stream(destination).await?;

	let (reader, writer) = tokio::io::split(stream);
	let mut client = cborpc::Client::from_async_stream(reader, writer);

	log::info!("Writing message");
	let msg = Message { text: message.into() };
	let msg_bytes = rmp_serde::encode::to_vec(&msg).unwrap();
	let call = cborpc::MethodCall {
		protocol_name: "pizdezh-0".to_string(),
		method_name: "send-message".to_string(),
		message: msg_bytes,
	};
	let _cr = client.call(&call).await?;

	Ok(())
}

#[tokio::main]
async fn main() -> Result<()> {
	let options = Options::from_args();

	match options {
		Options::Listen => server().await,
		Options::SendMessage { device, message } => client(&device, &message).await,
	}
}
